use err_derive::Error;
use std::io;

use log::error;

#[derive(Debug, Error)]
pub enum Error {
	#[error(display = "IO error: {}", _0)]
	Io(#[error(source)] io::Error),

	#[error(display = "Messagepack encode error: {}", _0)]
	RMPEncode(#[error(source)] rmp_serde::encode::Error),
	#[error(display = "Messagepack decode error: {}", _0)]
	RMPDecode(#[error(source)] rmp_serde::decode::Error),

	#[error(display = "Tokio join error: {}", _0)]
	TokioJoin(#[error(source)] tokio::task::JoinError),

	#[error(display = "oneshot receive error: {}", _0)]
	OneshotRecv(#[error(source)] tokio::sync::oneshot::error::RecvError),

	#[error(display = "Handshake error: {}", _0)]
	Handshake(#[error(source)] kuska_handshake::async_std::Error),

	#[error(display = "{}", _0)]
	Message(String),

	#[error(display = "Remote error: {}", _0)]
	Remote(String),
}

impl<T> From<tokio::sync::watch::error::SendError<T>> for Error {
	fn from(_e: tokio::sync::watch::error::SendError<T>) -> Error {
		Error::Message(format!("Watch send error"))
	}
}

impl<T> From<tokio::sync::mpsc::error::SendError<T>> for Error {
	fn from(_e: tokio::sync::mpsc::error::SendError<T>) -> Error {
		Error::Message(format!("MPSC send error"))
	}
}

/// Ths trait adds a `.log_err()` method on `Result<(), E>` types,
/// which dismisses the error by logging it to stderr.
pub trait LogError {
	fn log_err(self, msg: &'static str);
}

impl<E> LogError for Result<(), E>
where
	E: Into<Error>,
{
	fn log_err(self, msg: &'static str) {
		if let Err(e) = self {
			error!("Error: {}: {}", msg, Into::<Error>::into(e));
		};
	}
}
